from flask import Flask, request
import requests
import json
import time
ts = str(time.time()).split('.')[0]

app = Flask(__name__)
voipNumber = '00492018477685'

# @app.route('/call/<callid>/<number>/<pascom_token>/<zammad_token>', methods=['GET'])
# def call_pascom(callid, number, pascom_token, zammad_token):
#     if not number.startswith('00'):
#         number = '00%s' % number
    
#     direction = 1 if number != voipNumber else 0
#     print(direction, number)
#     payload = {
#         'event': 'newCall',
#         'callId':  callid,
#         'from': number if direction == 1 else voipNumber,
#         'to': voipNumber if direction == 1 else number,
#         'direction': 'in' if direction == 1 else 'out',
#         'user': ['safaa', voipNumber]
#     }
#     sendNotification(payload, zammad_token)
#     return 'ok'

def get_right_cdr(records, caller_id):
    if len(records) == 0:
        print('no current call')
        return
    if len(records) > 1:
        for record in records:
            if str(caller_id) in str(record.get('srcNumber', '')) or str(caller_id) in str(record.get('dstNumber', '')):
                return record
            if str(record.get('srcNumber', '')) in str(caller_id) or str(record.get('dstNumber', '')) in str(caller_id):
                return record
    return records[0]

@app.route('/v2/call/<caller_id>/<callid>/<pascom_token>/<zammad_token>', methods=['GET'])
def call_pascom_v2(caller_id, callid, pascom_token, zammad_token):

    url = 'https://pascom.cloud/schwedv/services/cdr/live'
    headers = {'Authorization': 'Basic %s' % pascom_token}
    response = requests.get(url, headers=headers)
    payload = get_right_cdr(response.json(), caller_id)
    
    srcNumber = payload.get('srcNumber')
    dstNumber = payload.get('dstNumber')
    srcInternal = payload.get('srcInternal')
    srcName = payload.get('srcName')
    dstName = payload.get('dstName')
    livecallid = payload.get('callId')
    
    direction = 0 if srcInternal else 1

    name = srcName if srcInternal else dstName
    number = dstNumber if srcInternal else srcNumber

    payload = {
        'event': 'newCall',
        'callId':  callid,
        'from':  voipNumber if srcInternal else srcNumber,
        'to': dstNumber if srcInternal else voipNumber,
        'direction': 'in' if direction == 1 else 'out',
        'user': [name]
    }
    sendNotification(payload, zammad_token)
    return str(livecallid)


# @app.route('/hangup/<ts>/<callid>/<number>/<pascom_token>/<zammad_token>', methods=['GET'])
# def hangup_pascom(ts, callid, number, pascom_token, zammad_token):
#     time.sleep(50)
#     if len(number) == 12:
#         number = '49' + number[1:]
#     if not number.startswith('00'):
#         number = '00%s' % number
#     ts = '%s000' % ts
#     url = 'https://pascom.cloud/schwedv/services/cdr/?limit=1&number=%s' % (number)
#     headers = {'Authorization': 'Basic %s' % pascom_token}
#     response = requests.get(url, headers=headers)
#     payload = response.json()
#     if(len(payload) == 0):
#         #1574967205
#         # number = '00%s' % (number)
#         # direction = 1 if number != voipNumber else 0
#         # # send no active agent
#         # payload = {
#         #     'event': 'hangup',
#         #     'cause': 'notFound',
#         #     'callId': callid,
#         #     'from': number if direction == 1 else voipNumber,
#         #     'to': voipNumber if direction == 1 else number,
#         #     'direction': 'in' if direction == 1 else 'out',
#         #     'answeringNumber': 1
#         # }
#         # sendNotification(payload, zammad_token)
#         return 'ok'
    
#     callRecord = payload[0]
#     if callRecord.get('result') == 'noanswer':
#         if callRecord.get('resultDetails') == 'abandon':
#             if int(callRecord.get('duration')) > 80:
#                  # send no answer
#                 send_hangup(callRecord, 'noAnswer', zammad_token, callid)
#             else:
#                 # send hangup event cancel
#                 send_hangup(callRecord, 'cancel', zammad_token, callid)
#         elif callRecord.get('resultDetails') == '':
#             print('send no answer unknown')
#             # send canceled by caller
#             send_hangup(callRecord, 'congestion', zammad_token,callid)
#     elif callRecord.get('result') == 'hangup':
#         # send answer
#         payload = {
#             'event': 'answer',
#             'callId': callid,
#             'from': callRecord.get('number') if callRecord.get('inbound') == 1 else voipNumber,
#             'to': voipNumber if callRecord.get('inbound') == 1 else callRecord.get('number'),
#             'direction': 'in' if callRecord.get('inbound') == 1 else 'out',
#             'answeringNumber': 1
#         }
#         sendNotification(payload, zammad_token)

#     elif callRecord.get('result') == 'transfer':
#         # send hangup event forwarded
#         send_hangup(callRecord, 'forwarded', zammad_token, callid)
#     return 'ok'


@app.route('/v2/hangup/<livecallid>/<callid>/<pascom_token>/<zammad_token>', methods=['GET'])
def hangup_pascom_v2(livecallid, callid, pascom_token, zammad_token):
    url = 'https://pascom.cloud/schwedv/services/cdr/live/%s' % (livecallid)
    headers = {'Authorization': 'Basic %s' % pascom_token}
    response = requests.get(url, headers=headers)
    payload = response.json()
    if not payload or not payload.get('callId'):
        #1574967205
        # number = '00%s' % (number)
        # direction = 1 if number != voipNumber else 0
        # # send no active agent
        # payload = {
        #     'event': 'hangup',
        #     'cause': 'notFound',
        #     'callId': callid,
        #     'from': number if direction == 1 else voipNumber,
        #     'to': voipNumber if direction == 1 else number,
        #     'direction': 'in' if direction == 1 else 'out',
        #     'answeringNumber': 1
        # }
        # sendNotification(payload, zammad_token)
        return 'no active call record exists'
    
    callRecord = payload
    if callRecord.get('connected') == 0:
        if int(callRecord.get('ringing')) < 80:
            send_hangup(callRecord, 'cancel', zammad_token, callid)
        else:
            send_hangup(callRecord, 'noAnswer', zammad_token, callid)
    else:
        srcNumber = callRecord.get('srcNumber')
        dstNumber = callRecord.get('dstNumber')
        srcInternal = callRecord.get('srcInternal')
        
        direction = 0 if srcInternal else 1
        # send answer
        payload = {
            'event': 'answer',
            'callId': callid,
            'from':  voipNumber if srcInternal else srcNumber,
            'to': dstNumber if srcInternal else voipNumber,
            'direction': 'in' if direction == 1 else 'out',
            'answeringNumber': 1
        }
        sendNotification(payload, zammad_token)
    return 'ok'

def send_hangup(callRecord, cause, zammad_token, callid):
    srcNumber = callRecord.get('srcNumber')
    dstNumber = callRecord.get('dstNumber')
    srcInternal = callRecord.get('srcInternal')
    
    direction = 0 if srcInternal else 1
    payload = {
        'event': 'hangup',
        'cause': cause,
        'callId':callid,
        'from':  voipNumber if srcInternal else srcNumber,
        'to': dstNumber if srcInternal else voipNumber,
        'direction': 'in' if direction == 1 else 'out',
        'answeringNumber': 1
    }
    sendNotification(payload, zammad_token)

def sendNotification(data={}, zammad_token=''):
    API_ENDPOINT = "https://zammad.schwedv.de/api/v1/cti/%s" % zammad_token
    # sending post request and saving response as response object 
    response = requests.post(url = API_ENDPOINT, data = data) 
    print(response.content)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug = True)
